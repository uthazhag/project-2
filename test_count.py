# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 00:07:36 2022

@author: uthay
"""

import unittest
import count
from string import ascii_lowercase,ascii_uppercase

# Assigning all the variables required for testing.
freq_dict = dict()
freq_letter =list()
file_list = list()
keys_to_delete = list()
lowercase_letters = list()
uppercase_letters = list()
for letter in ascii_lowercase:
    lowercase_letters.append(letter)
for letter in ascii_uppercase:
    uppercase_letters.append(letter)
class TestCount(unittest.TestCase):
    # test_frequencies function tests whether the add_frequencies function in count.py counts the frequencies of all the alphabets correctly for a 
    # given file. Please enter any txt filename in the place of 'line1.txt','line2.txt' and 'line3.txt'.
    def test_frequencies(self):
        self.assertEqual(count.add_frequencies(freq_dict, 'test.txt', True ),{'a':2,'b':2,'c':2,'d':2})
        # Clearing the dictionary so that the next test can run correctly using the same variable.
        freq_dict.clear()
        self.assertEqual(count.add_frequencies(freq_dict, 'test.txt', False ),{'A':2,'b':2,'C':1,'c':1,'d':1,'D':1})
        freq_dict.clear()
       
    
    # test_parseCargument function tests whether the parse_c_argument function in count.py detects the '-c' arguemnt correctly.
    def test_parseCargument(self):
        self.assertEqual(count.parse_c_argument(['-c','-l','abcd','line1.txt']),False)
        self.assertEqual(count.parse_c_argument(['-l','abcd','-z','line1.txt','line2.txt']),True)
        self.assertEqual(count.parse_c_argument(['-l','abcd','-z','line1.txt','line2.txt','-c']),False)
    
    # test_parseLargument function tests whether the parse_l_argument function in count.py correctly collects all the letters next to '-l' 
    # argument that the user wishes to find the frequency for.
    def test_parseLargument(self):
        self.assertEqual(count.parse_l_argument(['-c','-l','abcd','line1.txt'],freq_letter),['a','b','c','d'])
        freq_letter.clear()
        self.assertEqual(count.parse_l_argument(['-c','-l','efghi','line1.txt'],freq_letter),['e','f','g','h','i'])
        freq_letter.clear()
        self.assertEqual(count.parse_l_argument(['-l','xyz','-c','line1.txt'],freq_letter),['x','y','z'])
    
    # test_CollectFiles function tests whether the collect_filenames function in count.py correctly collects all the txt filenames from sys.argv.
    def test_CollectFiles(self):
        self.assertEqual(count.collect_filenames(['-c','-l','abcd','test.txt'],file_list),['test.txt'])
        file_list.clear()
        self.assertEqual(count.collect_filenames(['test.txt','-c','-l','abcd'],file_list),['test.txt'])
        file_list.clear()
        self.assertEqual(count.collect_filenames(['-z','test.txt','-l','abcd','line1.txt'],file_list),['test.txt','line1.txt'])
        
    
    # test_parseZargument function tests whether the parse_z_argument function in count.py correctly adds additional alphabets/keys to the frequency 
    # dictionary if the '-z' argument is present in sys.argv
    def test_parseZargument(self):
        self.assertEqual(count.parse_z_argument({'a':4,'b':2},['-c','-l','abcd','line1.txt'],True, lowercase_letters, uppercase_letters),{'a':4,'b':2})
        lowercase_letters.clear()
        uppercase_letters.clear()
        for letter in ascii_lowercase:
            lowercase_letters.append(letter)
        for letter in ascii_uppercase:
            uppercase_letters.append(letter)
        self.assertEqual(count.parse_z_argument({'a':4,'b':2},['-z','-c','-l','abcd','line1.txt'],False, lowercase_letters, uppercase_letters),{'a':4,'b':2,'c':0,'d':0,'e':0,'f':0,'g':0,'h':0,'i':0,'j':0,'k':0,'l':0,'m':0,'n':0,'o':0,'p':0,'q':0,'r':0,'s':0,'t':0,'u':0,'v':0,'w':0,'x':0,'y':0,'z':0,'A':0,'B':0,'C':0,'D':0,'E':0,'F':0,'G':0,'H':0,'I':0,'J':0,'K':0,'L':0,'M':0,'N':0,'O':0,'P':0,'Q':0,'R':0,'S':0,'T':0,'U':0,'V':0,'W':0,'X':0,'Y':0,'Z':0})
        lowercase_letters.clear()
        uppercase_letters.clear()
        for letter in ascii_lowercase:
            lowercase_letters.append(letter)
        for letter in ascii_uppercase:
            uppercase_letters.append(letter)
        self.assertEqual(count.parse_z_argument({'a':4,'b':2},['-z','-l','efgh','line1.txt'],True, lowercase_letters, uppercase_letters),{'e':0,'f':0,'g':0,'h':0,'a':4,'b':2,'c':0,'d':0,'i':0,'j':0,'k':0,'l':0,'m':0,'n':0,'o':0,'p':0,'q':0,'r':0,'s':0,'t':0,'u':0,'v':0,'w':0,'x':0,'y':0,'z':0})
    
    # test_SecondLParse function tests whether the second_l_parsing function in count.py correctly adds and deletes alphabets/keys that were collected
    # using parse_l_argument function from the frequency dictionary.
    def test_SecondLParse(self):
        self.assertEqual(count.second_l_parsing({'a':4,'c':3,'f':5},['-z','-c','line1.txt'],['a','c','t'],keys_to_delete),{'a':4,'c':3,'f':5})
        keys_to_delete.clear()
        self.assertEqual(count.second_l_parsing({'a':4,'c':3,'f':5},['-l','-z','-c','line1.txt'],['a','c','t'],keys_to_delete),{'a':4,'c':3,'t':0})
        keys_to_delete.clear()
        self.assertEqual(count.second_l_parsing({'a':4,'c':3,'f':5,'g':6,'h':7},['-l','-z','-c','line1.txt'],['a','c','g'],keys_to_delete),{'a':4,'c':3,'g':6})
        keys_to_delete.clear()
        self.assertEqual(count.second_l_parsing({'a':4,'c':3,'f':5},['-l','-z','-c','line1.txt'],['a','c','f','x','y','z'],keys_to_delete),{'a':4,'c':3,'f':5,'x':0,'y':0,'z':0})
if __name__ == '__main__':
    unittest.main()

    