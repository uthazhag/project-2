# -*- coding: utf-8 -*-
# Student name: A.P.Uthayashankar
# DU ID: 873595467
# This program prints the frequencies of every character present in the text files given by the user.
# There are three arguments with which the user can customize the result according to their wish.
import sys
import re
from string import ascii_lowercase,ascii_uppercase
# add_frequencies is a function that counts the frequencies of all the alphabets present in the files and writes it to a dictionary.
def add_frequencies(d,file,remove_case):
    ''' This function counts the frequencies of all the alphabets 
        present in a file and writes it to a dictionary and it 
        returns that dictionary.
    '''
    with open(file,'r') as f:
        contents = f.read()
        if remove_case == False:
            for letter in contents:
                if (letter not in ascii_uppercase) and (letter not in ascii_lowercase):
                    continue
                if letter in d:
                    d[letter] = d[letter] + 1
                else:
                    d[letter] = 1
        else:
            for letter in contents:
                if (letter.lower() not in ascii_lowercase):
                    continue
                if letter.lower() in d:
                    d[letter.lower()] = d[letter.lower()] + 1
                else:
                    d[letter.lower()] = 1
    return d

# parse_c_argument function parses the '-c' argument from sys.argv and assigns 'False' to 'remove_case' variaiable if '-c' is present in sys.argv.
def parse_c_argument(arguments_list):
    if '-c' in arguments_list:
        globals()['remove_case'] = False
    else:
        globals()['remove_case'] = True
    return globals()['remove_case']

# parse_l_argument function parses the '-l' argument from sys.argv and extracts all the letters that the user wishes to know the frequency of next
# to it and adds them in a separate list if the '-l' argument is present in sys.argv.
def parse_l_argument(arguments_list,freq_letters):
    for val in arguments_list:
        if val == '-l':
            l_arg_letters = arguments_list[arguments_list.index(val)+1]
            for letter in l_arg_letters:
                freq_letters.append(letter)
    return freq_letters

# collect_filenames function extracts all the '.txt' files from sys.argv and stores them in a separate list.
def collect_filenames(arguments_list, file_list):
    pattern = "\.txt$"
    for val in arguments_list:
        if re.search(pattern, val):
            file_list.append(val)
    return file_list

# After add_frequencies function finishes its job, parse_z_argument function parses the '-z' argument and adds all the additional alphabets to the 
# frequency dictionary depending on '-c' argument if '-z' is present in sys.argv. 
def parse_z_argument(d,arguments_list,remove_case,lowercase_letters,uppercase_letters):
    if '-z' in arguments_list:
        if remove_case == False:
            for key in d:
                if key in lowercase_letters:
                    lowercase_letters.remove(key)
                    continue
                if key in uppercase_letters:
                    uppercase_letters.remove(key)
            for letter in lowercase_letters:
                d[letter] = 0
            for letter in uppercase_letters:
                d[letter] = 0
        else:
            for key in d:
                if key in lowercase_letters:
                    lowercase_letters.remove(key)
            for letter in lowercase_letters:
                d[letter] = 0
    return d

# second_l_parsing function deletes all the unnecessary keys/alphabets from the frequency dictionary as wished by the user after add_frequencies 
# function finishes its job.
def second_l_parsing(d,arguments_list,freq_letters,keys_to_delete):
    if '-l' in arguments_list:
        for key in d:
            if key not in freq_letters:
                keys_to_delete.append(key)
        for letter in freq_letters:
            if letter not in d:
                d[letter] = 0
    for key in keys_to_delete:
        del d[key]
    return d
def print_result():
    print("Frequencies of characters in all the files is:")
    for key,val in freq.items():
        print(f"{key},{val}")
def main():
    global lowercase_letters 
    global uppercase_letters
    global remove_case 
    global freq_letters
    global files
    global keys_to_delete
    global freq
    lowercase_letters = list()
    uppercase_letters = list()  
    remove_case = True
    freq_letters = list()
    files = list()
    keys_to_delete = list()
    freq = dict()
    print(sys.argv)
    sys.argv.pop(0)
    parse_c_argument(sys.argv)
    parse_l_argument(sys.argv,freq_letters)
    collect_filenames(sys.argv, files)
    for letter in ascii_lowercase:
        lowercase_letters.append(letter)
    for letter in ascii_uppercase:
        uppercase_letters.append(letter)
    for file in files:
        add_frequencies(freq,file,remove_case)
    second_l_parsing(freq,sys.argv,freq_letters,keys_to_delete)
    parse_z_argument(freq,sys.argv,remove_case,lowercase_letters,uppercase_letters)
    print_result()
if __name__ == '__main__':
    main()
    



        
        
