# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 14:56:23 2022

@author: uthay
"""
import count
import sys
import re
import csv
count.main()
# Parsing the argument to get the csv filename enetred by the user:
csv_pattern = '\.csv$'
csv_filename = ''
for val in sys.argv:
    if re.search(csv_pattern, val):
        csv_filename = val

# Writing the frequencies of the alphabets to the csv file enetred by the user:
with open(csv_filename,'w') as csv_file:
    csv_writer = csv.writer(csv_file)
    for key,val in count.freq.items():
        tup = (key,val)
        csv_writer.writerow(tup)
